import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Query,
} from '@nestjs/common';
import { Batch } from '@prisma/client';

import {
  DEFAULT_PAGE_NUMBER,
  DEFAULT_PAGE_SIZE,
  PagerDto,
} from 'src/core/dto/pager.dto';
import { ReplyDto } from 'src/core/dto/reply.dto';

import { BatchCreateDto } from './dto/batch-create.dto';
import { BatchesService } from './batches.service';

@Controller({ path: 'batches', version: '1' })
export class BatchesController {
  constructor(private readonly batchesService: BatchesService) {}

  @Get()
  async find(
    @Query() pagerDto: PagerDto = new PagerDto(),
  ): Promise<ReplyDto<Batch[]>> {
    const pageNumber =
      pagerDto.pageNumber || pagerDto.page?.number || DEFAULT_PAGE_NUMBER;
    const pageSize =
      pagerDto.pageSize || pagerDto.page?.size || DEFAULT_PAGE_SIZE;

    const params = {
      take: pageSize,
      skip: pageSize * (pageNumber - 1),
      where: undefined,
    };

    const filterSearch = pagerDto.filterSearch || pagerDto.filter?.search;
    if (!!filterSearch) {
      params.where = {
        name: { contains: filterSearch },
      };
    }

    const batches = await this.batchesService.batches(params);
    const total = await this.batchesService.count({ where: params.where });

    return {
      data: batches,
      meta: { page: { number: pageNumber, size: pageSize, total: total } },
    };
  }

  @Post()
  async batchCreate(
    @Body() batchCreateDto: BatchCreateDto,
  ): Promise<ReplyDto<Batch>> {
    const data = {
      ...batchCreateDto,
    };

    const batch = await this.batchesService.createBatch(data);

    return { data: batch };
  }

  @Get(':id')
  async findOne(
    @Param('id', new ParseIntPipe())
    id: number,
  ): Promise<ReplyDto<Batch | null>> {
    const batch = await this.batchesService.batch({ id });

    return { data: batch };
  }
}
