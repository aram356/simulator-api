import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/core/prisma/prisma.service';

import { Prisma, Batch } from '@prisma/client';
import { SimsService } from 'src/sims/sims.service';
import { luhnChecksum } from 'src/core/utils/checksum';

@Injectable()
export class BatchesService {
  constructor(
    private prisma: PrismaService,
    private simsService: SimsService,
  ) {}

  async createBatch(data: Prisma.BatchCreateInput): Promise<Batch> {
    const batch = await this.prisma.batch.create({
      data,
    });

    const startIccid = BigInt(data.startIccid.substring(0, 19));
    const startImsi = parseInt(data.startImsi);

    for (let i = 0; i < data.count; i++) {
      const iccid = (startIccid + BigInt(i + 1)).toString();
      const imsi = (startImsi + i + 1).toString();

      const simInput = {
        iccid: `${iccid}${luhnChecksum(iccid)}`,
        imsi: imsi,
        isActive: data.isActive,
        batch: {
          connect: {
            id: batch.id,
          },
        },
      };

      await this.simsService.createSim(simInput);
    }

    return batch;
  }

  async batches(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.BatchWhereUniqueInput;
    where?: Prisma.BatchWhereInput;
    orderBy?: Prisma.BatchOrderByWithRelationInput;
  }): Promise<Batch[]> {
    const { skip, take, cursor, where, orderBy } = params;
    return this.prisma.batch.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
    });
  }

  async batch(
    batchWhereUniqueInput: Prisma.BatchWhereUniqueInput,
  ): Promise<Batch | null> {
    return this.prisma.batch.findUnique({
      where: batchWhereUniqueInput,
    });
  }

  async count(params: { where?: Prisma.BatchWhereInput }): Promise<number> {
    return await this.prisma.batch.count(params);
  }
}
