import {
  IsBoolean,
  IsInt,
  IsString,
  Max,
  Min,
  Validate,
} from 'class-validator';
import { IccidValidator } from 'src/core/validators/iccid.validator';
import { ImsiValidator } from 'src/core/validators/imsi.validator';

export class BatchCreateDto {
  @IsString()
  readonly name: string;

  @IsString()
  @Validate(IccidValidator, {
    message: 'startIccid is not valid',
  })
  readonly startIccid: string;

  @IsString()
  @Validate(ImsiValidator, {
    message: 'startImsi is not valid',
  })
  readonly startImsi: string;

  @IsInt()
  @Max(25)
  @Min(1)
  readonly count: number;

  @IsBoolean()
  readonly isActive: boolean;
}
