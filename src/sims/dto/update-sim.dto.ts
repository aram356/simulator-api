import { IsBoolean, IsString, Validate } from 'class-validator';

import { ImsiValidator } from 'src/core/validators/imsi.validator';
export class UpdateSimDto {
  @IsString()
  @Validate(ImsiValidator, {
    message: 'imsi is not valid',
  })
  readonly imsi: string;

  @IsBoolean()
  readonly isActive: boolean;
}
