import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/core/prisma/prisma.service';

import { Sim, Prisma } from '@prisma/client';

@Injectable()
export class SimsService {
  constructor(private prisma: PrismaService) {}

  async createSim(data: Prisma.SimCreateInput): Promise<Sim> {
    return this.prisma.sim.create({
      data,
    });
  }

  async sims(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.SimWhereUniqueInput;
    where?: Prisma.SimWhereInput;
    orderBy?: Prisma.SimOrderByWithRelationInput;
  }): Promise<Sim[]> {
    const { skip, take, cursor, where, orderBy } = params;
    return this.prisma.sim.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
    });
  }

  async sim(
    simWhereUniqueInput: Prisma.SimWhereUniqueInput,
  ): Promise<Sim | null> {
    return this.prisma.sim.findUnique({
      where: simWhereUniqueInput,
    });
  }

  async updateSim(params: {
    where: Prisma.SimWhereUniqueInput;
    data: Prisma.SimUpdateInput;
  }): Promise<Sim | null> {
    const { data, where } = params;
    return this.prisma.sim.update({
      data,
      where,
    });
  }

  async count(params: { where?: Prisma.SimWhereInput }): Promise<number> {
    return await this.prisma.sim.count(params);
  }
}
