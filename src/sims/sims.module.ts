import { Module } from '@nestjs/common';

import { PrismaService } from 'src/core/prisma/prisma.service';

import { SimsController } from './sims.controller';
import { SimsService } from './sims.service';

@Module({
  controllers: [SimsController],
  providers: [SimsService, PrismaService],
})
export class SimsModule {}
