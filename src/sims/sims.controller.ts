import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Put,
  Query,
} from '@nestjs/common';
import { Sim } from '@prisma/client';

import {
  DEFAULT_PAGE_NUMBER,
  DEFAULT_PAGE_SIZE,
  PagerDto,
} from 'src/core/dto/pager.dto';
import { ReplyDto } from 'src/core/dto/reply.dto';
import { UpdateSimDto } from './dto/update-sim.dto';
import { SimsService } from './sims.service';

@Controller({ path: 'sims', version: '1' })
export class SimsController {
  constructor(private readonly simsService: SimsService) {}

  @Get()
  async find(
    @Query() pagerDto: PagerDto = new PagerDto(),
  ): Promise<ReplyDto<Sim[]>> {
    const pageNumber =
      pagerDto.pageNumber || pagerDto.page?.number || DEFAULT_PAGE_NUMBER;
    const pageSize =
      pagerDto.pageSize || pagerDto.page?.size || DEFAULT_PAGE_SIZE;

    const params = {
      take: pageSize,
      skip: pageSize * (pageNumber - 1),
      where: undefined,
    };

    const filterSearch = pagerDto.filterSearch || pagerDto.filter?.search;
    if (!!filterSearch) {
      params.where = {
        OR: [
          { iccid: { contains: filterSearch } },
          { imsi: { contains: filterSearch } },
        ],
      };
    }

    const sims = await this.simsService.sims(params);
    const total = await this.simsService.count({ where: params.where });

    return {
      data: sims,
      meta: { page: { number: pageNumber, size: pageSize, total: total } },
    };
  }

  @Get(':id')
  async findOne(
    @Param('id', new ParseIntPipe()) id: number,
  ): Promise<ReplyDto<Sim | null>> {
    const sim = await this.simsService.sim({ id });

    return { data: sim };
  }

  @Put(':id')
  async update(
    @Param('id', new ParseIntPipe()) id: number,
    @Body() updateSimDto: UpdateSimDto,
  ): Promise<ReplyDto<Sim | null>> {
    const sim = await this.simsService.updateSim({
      where: { id: Number(id) },
      data: { ...updateSimDto },
    });

    return { data: sim };
  }
}
