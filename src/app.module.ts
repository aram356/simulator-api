import { Module } from '@nestjs/common';
import { BatchesModules } from './batches/batches.module';
import { CoreModule } from './core/core.module';
import { PrismaModule } from './core/prisma/prisma.module';
import { HealthModule } from './health/health.module';
import { SimsModule } from './sims/sims.module';

@Module({
  imports: [PrismaModule, BatchesModules, CoreModule, HealthModule, SimsModule],
})
export class AppModule {}
