import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

const IMSI_REG_EX = /^[\d]{15}$/;

@ValidatorConstraint()
export class ImsiValidator implements ValidatorConstraintInterface {
  validate(text: string) {
    return !!text && text.match(IMSI_REG_EX) != null;
  }
}
