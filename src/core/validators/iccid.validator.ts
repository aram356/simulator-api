import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

const ICCID_REG_EX = /^89[\d]{18}$/;

@ValidatorConstraint()
export class IccidValidator implements ValidatorConstraintInterface {
  validate(text: string) {
    return !!text && text.match(ICCID_REG_EX) != null;
  }
}
