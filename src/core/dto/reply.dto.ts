export class MetaPageDto {
  number: number;
  size: number;
  total: number;
}

export class MetaDto {
  page?: MetaPageDto;
}

export class ReplyDto<T> {
  data: T;
  meta?: MetaDto;
}
