import { Type } from 'class-transformer';
import { IsOptional, IsString } from 'class-validator';

export class FilterDto {
  @IsString()
  @IsOptional()
  @Type(() => Number)
  search: string;
}
