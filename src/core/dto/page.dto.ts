import { Type } from 'class-transformer';
import { IsInt, IsOptional, IsPositive, Max, Min } from 'class-validator';

export class PageDto {
  @IsInt()
  @IsOptional()
  @Min(1)
  @Type(() => Number)
  number: number;

  @IsInt()
  @IsPositive()
  @IsOptional()
  @Max(25)
  @Min(1)
  @Type(() => Number)
  size: number;
}
