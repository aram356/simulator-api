import { Type } from 'class-transformer';
import {
  IsInt,
  IsOptional,
  IsPositive,
  IsString,
  Max,
  Min,
  ValidateNested,
} from 'class-validator';

import { FilterDto } from './filter.dto';
import { PageDto } from './page.dto';

export const DEFAULT_PAGE_NUMBER = 1;
export const DEFAULT_PAGE_SIZE = 10;

export class PagerDto {
  @Type(() => PageDto)
  @ValidateNested()
  filter: FilterDto = new FilterDto();

  // Support both filter[search] and filterSearch
  @IsString()
  @IsOptional()
  @Type(() => Number)
  filterSearch: string;

  @Type(() => PageDto)
  @ValidateNested()
  page: PageDto = new PageDto();

  // Support both pageNumber and page[number]
  @IsInt()
  @IsOptional()
  @Min(1)
  @Type(() => Number)
  pageNumber: number;

  // Support both pageSize and page[size]
  @IsInt()
  @IsPositive()
  @IsOptional()
  @Max(25)
  @Min(1)
  @Type(() => Number)
  pageSize: number;
}
