import { randomInt } from 'crypto';

import { luhnChecksum } from './checksum';

export function randomStringOfInts(n: number): string {
  if (n < 1) {
    return null;
  }

  let result = `${randomInt(1, 9)}`;

  for (let i = 1; i < n; i++) {
    result = `${result}${randomInt(0, 9)}`;
  }

  return result;
}

export function randomImsi(): string {
  return `311480${randomStringOfInts(9)}`;
}

export function randomIccid(): string {
  const iccid = `891480${randomStringOfInts(13)}`;
  const checksum = luhnChecksum(iccid);

  return `${iccid}${checksum}`;
}
