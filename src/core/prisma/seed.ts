import { PrismaClient, Prisma } from '@prisma/client';

import { luhnChecksum } from 'src/core/utils/checksum';
import {
  randomIccid,
  randomImsi,
  randomStringOfInts,
} from 'src/core/utils/random';

const prisma = new PrismaClient();

async function main() {
  for (let b = 0; b < 2; b++) {
    const batchCreateInput: Prisma.BatchCreateInput = {
      name: `Sample Batch ${randomStringOfInts(10)}`,
      startIccid: randomIccid(),
      startImsi: randomImsi(),
      count: 25,
      isActive: true,
    };

    const batch = await prisma.batch.create({ data: batchCreateInput });

    for (let s = 0; s < batch.count; s++) {
      const iccid = (batch.startIccid + BigInt(s + 1)).toString();
      const imsi = (batch.startImsi + s + 1).toString();

      const simCreateInput: Prisma.SimCreateInput = {
        iccid: `${iccid}${luhnChecksum(iccid)}`,
        imsi: imsi,
        isActive: true,
        batch: {
          connect: {
            id: batch.id,
          },
        },
      };

      await prisma.sim.create({ data: simCreateInput });
    }
  }
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
