-- CreateTable
CREATE TABLE "Batch" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "name" TEXT NOT NULL,
    "startIccid" TEXT NOT NULL,
    "startImsi" TEXT NOT NULL,
    "count" INTEGER NOT NULL,
    "isActive" BOOLEAN NOT NULL,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- CreateTable
CREATE TABLE "Sim" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "iccid" TEXT NOT NULL,
    "imsi" TEXT NOT NULL,
    "isActive" BOOLEAN NOT NULL DEFAULT false,
    "batchId" INTEGER NOT NULL,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT "Sim_batchId_fkey" FOREIGN KEY ("batchId") REFERENCES "Batch" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);

-- CreateIndex
CREATE INDEX "Batch_name_idx" ON "Batch"("name");

-- CreateIndex
CREATE INDEX "Batch_startIccid_idx" ON "Batch"("startIccid");

-- CreateIndex
CREATE INDEX "Batch_startImsi_idx" ON "Batch"("startImsi");

-- CreateIndex
CREATE INDEX "Sim_iccid_idx" ON "Sim"("iccid");

-- CreateIndex
CREATE INDEX "Sim_imsi_idx" ON "Sim"("imsi");
